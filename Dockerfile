FROM python:3.6-alpine3.8
# TODO: Copy application files into container
COPY . /w_app
WORKDIR /w_app
# TODO: Install required python libraries
RUN pip3 install -r requirements.txt
# TODO: Have the container expose port 8080
EXPOSE 8080
ENTRYPOINT ["python3","/w_app/weather_b.py"]
