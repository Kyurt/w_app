#! /usr/bin/env python3
import requests
import argparse
import time
from flask import Flask

app = Flask(__name__)


current_time = int(time.time())
tomorrow_time = current_time + 3600


# Parser will figure out all the arguments
parser = argparse.ArgumentParser(description='Weather forecast for a given day') 
# Parser will except future or past days as argument
parser.add_argument('-n', '--ndays', type=int, help='provide day(s) for forecast, eg.: 3 (for 3 days ahead)', default=1)
# Parse any argument to the program
args = parser.parse_args()

DARK_SKY_SECRET_KEY="1d8c58ed1d54f96f939e706c788650f1"


latitude = "41.750183"
longitude = "32.387524" 
city = "Amasra, Turkey"
    
def get_time(frcst_date=tomorrow_time):
    """ Returns time for given date
    Return
    str: unix time
    """
    frcst_date = args.ndays * 3600 + current_time
    return frcst_date

@app.route("/")
def get_temperature(longitude=longitude, latitude=latitude):
    """ Returns the current temperature at the specified location
    """

    r = requests.get('https://api.darksky.net/forecast/{key}/{lat},{long},{f_time}'.format(
                    key=DARK_SKY_SECRET_KEY, lat=latitude, long=longitude, f_time=get_time()))


    forecast = r.json()
    temp = int(forecast['currently']['temperature'])
    summary = forecast['currently']['summary']
    humidity = "{:.0%}".format(forecast['currently']['humidity'])
    feels_like = int(forecast['currently']['apparentTemperature'])

    f_cast = {'Location':city,'Temperature':temp, 'Summary':summary, 'Humidity':humidity, 
              'Feels like':feels_like, 'Forecast day(s)':args.ndays}
    return f_cast 


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
