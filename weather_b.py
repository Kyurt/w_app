#! /usr/bin/env python3
import requests
import argparse
import time

current_time = int(time.time())
tomorrow_time = current_time + 3600


# Parser will figure out all the arguments
parser = argparse.ArgumentParser(description='Weather forecast for a given day') 
# Parser will except future or past days as argument
parser.add_argument('-n', '--ndays', type=int, help='provide day(s) for forecast, eg.: 3 (for 3 days ahead)', default=1)
# Parse any argument to the program
args = parser.parse_args()

DARK_SKY_SECRET_KEY="1d8c58ed1d54f96f939e706c788650f1"



def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude

    longitude = location['longitude']
    latitude = location['latitude']
    city = location['city']

    """
    rl = requests.get('https://ipapi.co/json')
    location = rl.json()

    latitude = "41.750183"
    longitude = "32.387524" 
    city = "Amasra, Turkey"


    return longitude, latitude, city
    
def get_time(frcst_date=tomorrow_time):
    """ Returns time for given date
    Return
    str: unix time
    """
    frcst_date = args.ndays * 3600 + current_time
    return frcst_date

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str): 
    latitude (str):

    Returns:
    float: temperature
    """
    r = requests.get('https://api.darksky.net/forecast/{key}/{lat},{long},{f_time}'.format(
                    key=DARK_SKY_SECRET_KEY, lat=latitude, long=longitude, f_time=frcst_date))

    forecast = r.json()
    temp = forecast['currently']['temperature']
    summary = forecast['currently']['summary']
    humidity = forecast['currently']['humidity']
    feels_like = forecast['currently']['apparentTemperature']
    return temp, summary, humidity, feels_like

def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """

    #print(f"Today's forecast is: {temp} degrees!")
    print(f'The weather forecast for {city} in {args.ndays} days \n')
    print(f'{summary}\n')
    print(f'Temprerature: {temp}')
    print(f'Humidity:     {humidity*100:.1f}%')
    print(f'Feels Like: {feels_like:7}')

if __name__ == "__main__":
    longitude, latitude, city = get_location()
    frcst_date = get_time()
    temp, summary, humidity, feels_like = get_temperature(longitude, latitude)
    print_forecast(temp)
